import json
from pathlib import Path
import datetime # Remove

"""
The repository class for books. Takes care of the database for books and has methods to return data
and change it.
"""
class BookRepository(object):    
    """ Returns all the books in the database """
    def get_all_books(self):
        with open(self.__get_path(), 'r') as rf:
            bookdata = json.loads(rf.read())
        return bookdata
    
    """ Returns a specific book that has the same id as the book_id input """
    def get_book(self, book_id):
       return self.get_all_books()[str(book_id)]

    """ Returns a specific book that has the same name as the name input """
    def get_book_by_name(self, name):
        for value in self.get_all_books().values():
            if value["name"] == name:
                return value
        return None
    
    """ Returns the id that the next book added to the database will have """
    def get_next_book_id(self):
        next_book_id = -1
        with open("app/data/ids.json", "r") as filestream:
            ids = json.loads(filestream.read())
            next_book_id = ids["next_book_id"]
        return next_book_id
    

    """ Adds a new book to the database """
    def add_book(self, book_dict):
        book_id = self.get_next_book_id()
        book_dict["id"] = book_id
        book_dict["status"] = 1
        all_books = self.get_all_books()
        all_books[book_id] = book_dict
        with open("app/data/bookdata.json", "w") as filestream:
            filestream.write(json.dumps(all_books))
        self.__increment_next_book_id()
        return book_dict
    
    """
        Updates an already existing book, throws an error if the 'id' in the
        book_dict parameter does not exist.
    """
    def update_book(self, book_dict):
        all_books = self.get_all_books()
        all_books[str(book_dict["id"])] = book_dict
        with open("app/data/bookdata.json", "w") as filestream:
            filestream.write(json.dumps(all_books))

    """ Removes a book from the database that has the same id as the bookid input """
    def remove_book(self, bookid):
        all_books = {}
        with open("app/data/bookdata.json", "r") as filestream:
            all_books = json.loads(filestream.read())
        all_books[str(bookid)]["status"] = 3
        with open("app/data/bookdata.json", "w") as filestream:
            filestream.write(json.dumps(all_books))
    
    """ Sets a book with the give ID to the given status """
    def change_book_availability(self, book_id, new_status):
        all_books = self.get_all_books()
        all_books[str(book_id)]["status"] = new_status
        with open("app/data/bookdata.json", "w") as filestream:
            filestream.write(json.dumps(all_books))
    
    """ Actually removes a book from the system (only used in tests) """
    def hard_delete_book(self, bookid):
        all_books = {}
        with open("app/data/bookdata.json", "r") as filestream:
            all_books = json.loads(filestream.read())
        try:
            del all_books[str(bookid)]
        except KeyError:
            pass
        with open("app/data/bookdata.json", "w") as filestream:
            filestream.write(json.dumps(all_books))
        
    """ Decrements next book id (For testing purposes only)"""
    def decrement_next_book_id(self):
        all_ids = {}
        with open("app/data/ids.json", "r") as filestream:
            all_ids = json.loads(filestream.read())
        all_ids["next_book_id"] -= 1
        with open("app/data/ids.json", "w") as filestream:
            filestream.write(json.dumps(all_ids))
    
    """ Checks if the book exsits """
    def book_exists(self, book_id):
        all_books = {}
        with open("app/data/bookdata.json", "r") as filestream:
            all_books = json.loads(filestream.read())
        return str(book_id) in all_books

    """ Returns the reletive path to json file"""
    def __get_path(self):
        base_path = Path(__file__).parent
        return (base_path / "../data/bookdata.json").resolve()
    
    """ Increments next book id """
    def __increment_next_book_id(self):
        ids = {}
        with open("app/data/ids.json", "r") as filestream:
            ids = json.loads(filestream.read())
        with open("app/data/ids.json", "w") as filestream:
            ids["next_book_id"] += 1
            filestream.write(json.dumps(ids))

''' book_repo = BookRepository()
book_repo.add_book({"book_id": 1,
            "user_id": 2,
            "name": "hello",
            "author": "my leg",
            "genre": "fantasy",
            "published": 1998,
            "condition": 2,
            "status": "available"}) '''