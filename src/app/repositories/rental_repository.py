import json
from pathlib import Path

"""
    A class that encapsulates the functionality of the program's rental
    repository
"""
class RentalRepository(object):

    def __init__(self):
        self.rentals = self.get_all_rentals()

    """ Gets the next available ID for a new rental object """
    def get_next_rental_id(self):
        all_ids = {}
        with open("app/data/ids.json", "r") as filestream:
            all_ids = json.loads(filestream.read())
        return all_ids["next_rental_id"]

    """ Reads and returns all the registered rentals."""
    def get_all_rentals(self):
        all_rentals = {}
        with open("app/data/rentaldata.json", "r") as filestream:
            all_rentals = json.loads(filestream.read())
        return all_rentals

    """ Returns a rental with the given ID."""
    def get_rental(self,rental_id):
        all_rentals = self.get_all_rentals()
        return all_rentals[str(rental_id)]

    """ Adds a new rental object to the data base."""
    def add_rental(self, rental_dict):
        all_rentals = self.get_all_rentals()
        rental_id = str(rental_dict["id"])
        all_rentals[rental_id] = rental_dict
        with open("app/data/rentaldata.json", "w") as filestream:
            filestream.write(json.dumps(all_rentals))
        self.increment_next_rental_id()
        return rental_dict
    
    """ Returns True if a rental with the given ID exists, False otherwise """
    def rental_exists(self, rental_id):
        all_rentals = self.get_all_rentals()
        return str(rental_id) in all_rentals
    
    """ Marks a rental with the given rental ID as 'closed' """
    def close_rental(self, rental_id):
        all_rentals = self.get_all_rentals()
        all_rentals[str(rental_id)]["status"] = 2
        with open("app/data/rentaldata.json", "w") as filestream:
            filestream.write(json.dumps(all_rentals))
    
    """ Increments next ID """
    def increment_next_rental_id(self):
        all_ids = {}
        with open("app/data/ids.json", "r") as filestream:
            all_ids = json.loads(filestream.read())
        all_ids["next_rental_id"] += 1
        with open("app/data/ids.json", "w") as filestream:
            filestream.write(json.dumps(all_ids))
    
    """ Decrements next ID. (For testing purpose only) """
    def decrement_next_rental_id(self):
        all_ids = {}
        with open("app/data/ids.json", "r") as filestream:
            all_ids = json.loads(filestream.read())
        all_ids["next_rental_id"] -= 1
        with open("app/data/ids.json", "w") as filestream:
            filestream.write(json.dumps(all_ids))
    
    """ Permanently removes a rental from the system, only use in test files! """
    def hard_delete_rental(self, rental_id):
        all_rentals = self.get_all_rentals()
        del all_rentals[str(rental_id)]
        with open("app/data/rentaldata.json", "w") as filestream:
            filestream.write(json.dumps(all_rentals))
