import asyncio
import websockets
import json
import sys

""" 
    Returns either the address for the local WebSocket server or the Heroku
    server depending on command line arguments.
"""
def get_server_uri(local=False):
    uri = "ws://bookflip.herokuapp.com/:8080"
    if ("local" in sys.argv) or local:
        uri = "ws://127.0.0.1:8080"
    return uri

async def msgSender():
    uri = get_server_uri()
    send_dict = {}
    async with websockets.connect(uri) as websocket:
        op = input("Operation: ")
        send_dict = {"op": op}
        if op == "get_user":
            user_id = input("User ID: ")
            send_dict = {"op": op, "id": user_id}
        await websocket.send(json.dumps(send_dict))
        return_val = await websocket.recv()
        print(f"< {return_val}")

""" Sends a message to the server with a given username """
async def send_test_message(body):
    uri = get_server_uri(local=True)
    async with websockets.connect(uri) as websocket:
        await websocket.send(json.dumps(body))
        return_val = await websocket.recv()
        return return_val

if __name__ == '__main__':
    #Run the msgSender() function until it is complete
    #This means it is run once only; the program ends afterwards
    #You could also loop this execution, like on the server (e.g., if you had a chat program)
    asyncio.get_event_loop().run_until_complete(msgSender())