import unittest
from datetime import datetime
from app.services.message_service import MessageService

""" Tests the backend's chat/messaging functionality """
class TestMessage(unittest.TestCase):
    """ A method that is run before every test """
    def setUp(self):
        self.__message_service = MessageService()
        message = {
            "sender_id": -1,
            "receiver_id": -2,
            "timestamp": "09/28/2019 13:00:00",
            "message": "That relatable moment when :|"
            }

        self.__dummy_message = self.__message_service.send_message(message)
    
    def tearDown(self):
        self.__message_service.hard_delete_message(self.__dummy_message["id"])
        self.__message_service.decrement_next_message_id()

    """ Tests whether send_message stores the new message correctly """
    def test_send_message_stores_new_message(self):
        message = self.__message_service.get_message(self.__dummy_message["id"])
        self.assertEqual(self.__dummy_message["message"], message["message"])
        self.assertEqual(self.__dummy_message["sender_id"], message["sender_id"])
        self.assertEqual(self.__dummy_message["receiver_id"], message["receiver_id"])
        self.assertEqual(self.__dummy_message["timestamp"], message["timestamp"])

    """
        Tests if send_message raises a TypeError if the message object given is
        not an instance of the Message class.
    """
    def test_send_message_raises_value_error_if_not_message(self):
        new_message = None
        with self.assertRaises(ValueError):
            self.__message_service.send_message(new_message)
    
    """
        Test if get_messages_to_user_id returns all the messages sent to the
        user with the specified ID.
    """
    def test_get_messages_to_user_id_returns_all_messages(self):
        all_messages = self.__message_service.get_messages_to(-2)
        for message in all_messages.values():
            self.assertEqual(message["receiver_id"], -2)

    """
        Test if get_messages_by_user_id returns all the messages written by the
        user with the given user ID.
    """
    def test_get_messages_by_user_id_returns_all_messages(self):
        messages = self.__message_service.get_messages_by(-1)
        for message in messages.values():
            self.assertEqual(message["sender_id"], -1)
    
    """
        Test if get_messages_from_a_to_b returns all the messages written by the
        user with ID=a sent to user with ID=b
    """
    def test_get_messages_from_a_to_b_returns_all_messages(self):
        messages = self.__message_service.get_messages_between(-1, -2)
        for message in messages.values():
            self.assertEqual(message["sender_id"], -1)
            self.assertEqual(message["receiver_id"], -2)


if __name__ == "__main__":
    unittest.main()