import unittest
import app.data.dummydata as dummydata
import app.services.book_service as BookFunctions
from app.services import book_service

""" Tests the get_book() function to make sure it is working as expected """
class testGetBook(unittest.TestCase):
    """ Sets up the test for each test method """
    def setUp(self):
        self.__book_service = book_service.BookService()
    
    """ Cleans up for the next test method """
    def tearDown(self):
        pass
    
    """ Test to see if the function gives the right book when given a title that exists """
    def test_get_book_exists(self):
        fake_title = "Harry Potter and the Chamber of Secrets"
        self.assertEqual(self.__book_service.get_book(fake_title), dummydata.book1)

    """ Test to see if the function gives the right response when book doesn't exist """
    def test_get_book_not_exists(self):
        fake_title = "According to all known laws of aviation"
        self.assertEqual(self.__book_service.get_book(fake_title), "Book doesn't exist!")
    
    """ Test to see if the function gives the right response when given an invalid title """
    def test_get_book_unexpected_input(self):
        fake_title = 25
        self.assertEqual(self.__book_service.get_book(fake_title), "Book doesn't exist!")

if __name__ == "__main__":
       unittest.main()